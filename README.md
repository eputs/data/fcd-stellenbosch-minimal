This taxi dataset is a modification of the dataset cited below ([Akpa 2016][1]), which was compiled at Electrical and Electronic Department, Stellenbosch University, 2015. 

Citation:

N.A. Ebot Eno Akpa, M.J. Booysen, M. Sinclair, "Publicly available annotated dataset of tracked taxis", 2016. Original URL: http://staff.ee.sun.ac.za/mjbooysen/TaxiData/. Archived at: https://gitlab.com/eputs/data/fcd-stellenbosch.


The dataset contains data of taxis tracked over **1 month** (June 2015), as extracted from [Akpa (2016)][1]. The tracking devices and data were sponsored by MiX Telematics, and the research by MTN as part of the MTN Mobile Intelligence Lab.

Please cite the appropriate papers from following if you wish to use the dataset:
 - N.A. Ebot Eno Akpa, M.J. Booysen, M. Sinclair, "Auditory Intelligent Speed Adaptation for long-distance informal public transport in South Africa", IEEE Intelligent Transport Systems Magazine -- accepted for publication in May 2016. 
 - N.A. Ebot Eno Akpa, M.J. Booysen, M. Sinclair, "Efficacy of Interventions and Incentives to Achieve Speed Compliance in the Informal Public Transport Sector", IEEE CIVTS, December 2015, Cape Town, South Africa
 - N.A. Ebot Eno Akpa, M.J. Booysen, M. Sinclair, "The impact of average speed over distance (ASOD) systems on speeding patterns along the R61", UMICTA, December 2014, Stellenbosch, South Africa.
 - I. Ndibatya, M.J. Booysen, "Modelling of inter-stop minibus taxi movements: Using machine learning and network theory", UMICTA, December 2014, Stellenbosch, South Africa.
 - I. Ndibatya, M.J. Booysen, J. Quinn, "An Adaptive Transportation Prediction Model for the Informal Public Transport Sector in Africa", IEEE ITSC, 2014, Qingdao, China.

The data is split per taxi, and contains the following columns of tracking data: 

GPSID; Time; Latitude; Longitude; Altitude; Heading; Satellites; HDOP; AgeOfReading; DistanceSinceReading; Velocity

[1]: https://gitlab.com/eputs/data/fcd-stellenbosch
